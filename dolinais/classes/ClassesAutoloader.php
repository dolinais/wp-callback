<?php

class ClassesAutoloader
{
    public $dirArray = [];

    public function __construct($app)
    {
        $this->getDirs($app);
    }

    public function getClass($className)
    {
        foreach ($this->dirArray as $path) {
            $filename = "{$path}/".basename(str_replace("\\", "/", $className)).".php";
            if (file_exists($filename)) {
                include $filename;
                break;
            }
        }
    }

    public function getDirs($dir)
    {
        $arr = glob("{$dir}/*", GLOB_ONLYDIR);
        if (count($arr)) {
            foreach ($arr as $dirname) {
                if (preg_match_all('/\b[A-Z][a-z]+/u', $dirname)) {
                    $this->dirArray[] = $dirname;
                    $this->getDirs($dirname);
                }
            }
        }
    }

    public function setLog($fileName, $data) {
        $fh = fopen($fileName, 'a');
        fwrite($fh, date('d-m-Y H:i:s') . " - " . $data . "\n");
        fclose($fh);
    }
}