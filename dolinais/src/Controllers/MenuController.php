<?php

namespace app\controllers;

defined( 'ABSPATH' ) || die( '-1' );

class MenuController {

	public function __construct(){
	    add_action('admin_menu', array($this, 'ActionMenu'));
  	}

  	function ActionMenu() {
	    add_menu_page("Заявки", "Заявки", "manage_options",
	        "dolina", array($this, 'Settings'), "dashicons-email-alt");
	    add_submenu_page("dolina", "Telegram", "Telegram", "manage_options",
        "dolinais", array($this, 'Settings'));
	    add_submenu_page("dolina", "Настроить кнопку", "Настроить кнопку", "manage_options",
	        "dolinais-settings", array($this, 'SettingsAdd'));
	    remove_submenu_page('dolina','dolina');
	}

	function Settings() {
		wp_enqueue_script( 'apiscript', RESOURCES . '/js/script.js', array(), '1.0.1' );
	    wp_enqueue_style( 'bootstrap@5.2.3', RESOURCES.'/style/npm_bootstrap@5.3.0_dist_css_bootstrap.min.css', array(), '5.3.0' );
	    wp_enqueue_style( 'popap_tsyle', RESOURCES.'/style/popap_style.css', array(), '1.0.0' );
	    wp_enqueue_script( 'bootstrap@5.3.0_dist_js', RESOURCES . '/js/bootstrap@5.3.0_dist_js_bootstrap.bundle.min.js', array(), '5.3.0' );
		render(get_class($this), 'index', null, null);
		render(get_class($this), 'addkey', null, null);
	}

	function SettingsAdd() {
		wp_enqueue_script( 'apiscript', RESOURCES . '/js/script.js', array(), '1.0.1' );
		wp_enqueue_style( 'bootstrap@5.2.3', RESOURCES.'/style/bootstrap.min.css' );
		render(get_class($this), 'settingstest', null, null);
	}
}