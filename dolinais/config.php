<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/**
 * The plugin bootstrap file
 *
/**
 * The admin-specific functionality of the plugin.
 *
 * @since      1.0.0
 *
 * @package    Call_Back_BOT
 * @subpackage Call_Back_BOT/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Call_Back_BOT
 * @subpackage Call_Back_BOT/admin
 * @author     Dolina IS
 */

if ( ! defined( 'WPINC' )
    && ! define('APP')
    && ! define('POSTURL')
    && ! define('RESOURCES')
    && ! define('TEMPLATES')){
	die;
} else{
    define('APP', dirname(__FILE__) . '/src');
    define('TEMPLATES', dirname(__FILE__) . '/src/templates');
    define('RESOURCES', plugins_url( '/resources', __FILE__ ));
    define('POSTURL', plugins_url( '/', __FILE__ ));
}

require_once dirname( __FILE__ ) . '/classes/ClassesAutoloader.php';
spl_autoload_register([new ClassesAutoloader(APP), 'getClass']);

function render($get_class_name, $__view, $data, $template){
    $obj = new app\renderCore\RenderCore($get_class_name, $__view, $data, $template);
    return $obj;
}
function shorcoderender($get_class_name, $__view, $data, $template){
    $obj = new app\shortcodeCore\ShortcodeCore($get_class_name, $__view, $data, $template);
    return $obj;
}

function tg($telegram_id, $data){
    $classtg = new app\models\TelegramModels();
    return $classtg->send(
        'sendMessage',
        array(
            'chat_id' => $telegram_id,
            'text' => $data,
            'parse_mode' => 'HTML'
        )
    );
}